<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body class="<?php print $body_classes; ?>">
  <div id="page" class="container-16 clear-block">

    <div id="site-header" class="clear-block">
      <div id="branding" class="grid-8 clear-block">
      <?php if ($linked_logo_img): ?>
        <span id="logo" class="grid-1 alpha"><?php print $linked_logo_img; ?></span>
      <?php endif; ?>
      <?php if ($is_front): ?>
        <div id="site-name" class="grid-7 omega"><h1><?php print $linked_site_name; ?></h1></div>
      <?php else: ?>
        <div id="site-name" class="grid-7 omega"><?php print $linked_site_name; ?></div>
      <?php endif; ?>
      <?php if ($site_slogan): ?>
        <div id="site-slogan" class="grid-8 alpha"><?php print $site_slogan; ?></div>
      <?php endif; ?>
      </div>

    <?php if ($main_menu_links || $secondary_menu_links): ?>
      <div id="site-menu" class="grid-8">
        <?php print $main_menu_links; ?>
        <?php print $secondary_menu_links; ?>
      </div>
    <?php endif; ?>

    <?php if ($search_box): ?>
      <div id="search-box" class="grid-6 prefix-10"><?php print $search_box; ?></div>
    <?php endif; ?>
    </div>


    <div id="site-subheader" class="grid-16 omega">
    <?php if ($mission): ?>
      <div id="mission">
        <?php print $mission; ?>
      </div>
    <?php endif; ?>

    <?php if ($headeroutside): ?>
      <div id="headeroutside-region">
        <?php print $headeroutside; ?>
      </div>
    <?php endif; ?>
    </div>


    <div id="main" class="column <?php print ns('grid-16', $leftthin1, 2, $leftthin2, 2, $leftthick1, 4, $leftthick2, 4, $leftthin3, 2, $leftthin4, 2, $rightthin1, 2, $rightthin2, 2, $rightthick1, 4, $rightthick2, 4, $rightthin3, 2, $rightthin4, 2) . ' ' . ns('push-16', !$leftthin1, 2, !$leftthin2, 2, !$leftthick1, 4, !$leftthick2, 4, !$leftthin3, 2, !$leftthin4, 2); ?>">
      <?php if ($headerinside): ?>
        <div id="headerinside-region">
          <?php print $headerinside; ?>
        </div>
      <?php endif; ?>
      <?php print $breadcrumb; ?>
      <?php if ($is_front): ?>
        <h2 class="title" id="page-title"><?php print $title; ?></h2>
      <?php else: ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php if ($tabs): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>
      <?php print $messages; ?>
      <?php print $help; ?>

      <div id="main-content" class="region clear-block">
        <?php print $content; ?>
      </div>

      <?php print $feed_icons; ?>

      <?php if ($footerinside): ?>
        <div id="footerinside-region">
          <?php print $footerinside; ?>
        </div>
      <?php endif; ?>
    </div>

  <?php if ($leftthin1): ?>
    <div id="sidebar-leftthin1" class="column sidebar region grid-2 <?php print ns('pull-16', $leftthin1, 2, $leftthin2, 2, $leftthick1, 4, $leftthick2, 4, $leftthin3, 2, $leftthin4, 2, $rightthin1, 2, $rightthin2, 2, $rightthick1, 4, $rightthick2, 4, $rightthin3, 2, $rightthin4, 2); ?>">
      <?php print $leftthin1; ?>
    </div>
  <?php endif; ?>

  <?php if ($leftthin2): ?>
    <div id="sidebar-leftthin2" class="column sidebar region grid-2 <?php print ns('pull-16', $leftthin1, 2, $leftthin2, 2, $leftthick1, 4, $leftthick2, 4, $leftthin3, 2, $leftthin4, 2, $rightthin1, 2, $rightthin2, 2, $rightthick1, 4, $rightthick2, 4, $rightthin3, 2, $rightthin4, 2); ?>">
      <?php print $leftthin2; ?>
    </div>
  <?php endif; ?>

  <?php if ($leftthick1): ?>
    <div id="sidebar-leftthick1" class="column sidebar region grid-4 <?php print ns('pull-16', $leftthin1, 2, $leftthin2, 2, $leftthick1, 4, $leftthick2, 4, $leftthin3, 2, $leftthin4, 2, $rightthin1, 2, $rightthin2, 2, $rightthick1, 4, $rightthick2, 4, $rightthin3, 2, $rightthin4, 2); ?>">
      <?php print $leftthick1; ?>
    </div>
  <?php endif; ?>

  <?php if ($leftthick2): ?>
    <div id="sidebar-leftthick2" class="column sidebar region grid-4 <?php print ns('pull-16', $leftthin1, 2, $leftthin2, 2, $leftthick1, 4, $leftthick2, 4, $leftthin3, 2, $leftthin4, 2, $rightthin1, 2, $rightthin2, 2, $rightthick1, 4, $rightthick2, 4, $rightthin3, 2, $rightthin4, 2); ?>">
      <?php print $leftthick2; ?>
    </div>
  <?php endif; ?>

  <?php if ($leftthin3): ?>
    <div id="sidebar-leftthin3" class="column sidebar region grid-2 <?php print ns('pull-16',$leftthin1, 2, $leftthin2, 2, $leftthick1, 4, $leftthick2, 4, $leftthin3, 2, $leftthin4, 2, $rightthin1, 2, $rightthin2, 2, $rightthick1, 4, $rightthick2, 4, $rightthin3, 2, $rightthin4, 2); ?>">
      <?php print $leftthin3; ?>
    </div>
  <?php endif; ?>

  <?php if ($leftthin4): ?>
    <div id="sidebar-leftthin4" class="column sidebar region grid-2 <?php print ns('pull-16', $leftthin1, 2, $leftthin2, 2, $leftthick1, 4, $leftthick2, 4, $leftthin3, 2, $leftthin4, 2, $rightthin1, 2, $rightthin2, 2, $rightthick1, 4, $rightthick2, 4, $rightthin3, 2, $rightthin4, 2); ?>">
      <?php print $leftthin4; ?>
    </div>
  <?php endif; ?>

  <?php if ($rightthin1): ?>
    <div id="sidebar-rightthin1" class="column sidebar region grid-2">
      <?php print $rightthin1; ?>
    </div>
  <?php endif; ?>

  <?php if ($rightthin2): ?>
    <div id="sidebar-rightthin2" class="column sidebar region grid-2">
      <?php print $rightthin2; ?>
    </div>
  <?php endif; ?>

  <?php if ($rightthick1): ?>
    <div id="sidebar-rightthin1" class="column sidebar region grid-4">
      <?php print $rightthick1; ?>
    </div>
  <?php endif; ?>

  <?php if ($rightthick2): ?>
    <div id="sidebar-rightthin2" class="column sidebar region grid-4">
      <?php print $rightthick2; ?>
    </div>
  <?php endif; ?>

  <?php if ($rightthin3): ?>
    <div id="sidebar-rightthin3" class="column sidebar region grid-2">
      <?php print $rightthin3; ?>
    </div>
  <?php endif; ?>

  <?php if ($rightthin4): ?>
    <div id="sidebar-rightthin4" class="column sidebar region grid-2">
      <?php print $rightthin4; ?>
    </div>
  <?php endif; ?>

  <div id="footeroutside" class="grid-16 omega">
    <?php if ($footeroutside): ?>
      <div id="footeroutside-region">
        <?php print $footeroutside; ?>
      </div>
    <?php endif; ?>

    <?php if ($footer_message): ?>
      <div id="footer-message">
        <?php print $footer_message; ?>
      </div>
    <?php endif; ?>
  </div>


  </div>
  <?php print $closure; ?>
</body>
</html>
