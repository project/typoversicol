<?php
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes ?>">
<?php print $picture ?>
<?php if (!$page): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>" class="node-title"><?php print $title ?></a></h2>
<?php endif; ?>
    <span class="submitted"><?php print $submitted; ?></span>
  <div class="content clearfix">
    <?php print $content ?>
  </div>
  <div class="meta">
    <div class="terms"><?php print $terms ?></div>
    <div class="links"><?php print $links; ?></div>
  </div>
  <?php print $comments; ?>
</div>
